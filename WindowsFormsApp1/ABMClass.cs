﻿using System;
using ODataClientCodeGeneratorSample;
using System.Configuration;
using System.Windows.Forms;
using System.Text;
using System.Linq;
using System.Drawing;


namespace Functionalities {


    public static class ABMClass
    {
        // Singleton de contenedor de datos
        public static DefaultContainer Connector()
        {

            // Trae la informacion de odata
            var context = new DefaultContainer(new Uri(ConfigurationManager.AppSettings["URL"]));

            return context;
        }
    }

}
