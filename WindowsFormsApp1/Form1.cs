﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.OData.Client;
// Defino el namespace creado con el Odata Generator
using System.Text.RegularExpressions;
using ODataClientCodeGeneratorSample;
using Functionalities;
using System.Configuration;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        // Variable para guardar valor inicial del indice (Solo para no seleccionar la barra de titulo)
        private int n = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Cuando inicia la aplicacion trae toda la informacion de odata
            LoadDB();
        }




        // Declaro la funcion del boton adicionar campo
        private void button1_Click(object sender, EventArgs e)
        {
            
            // vuelve a cargar los datos de la DB
            // LoadDB();


            Regex regex = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isValid = regex.IsMatch(txtEmail.Text.Trim());
            if (!isValid)
            {
                MessageBox.Show("Formato de email invalido.");
            }
            else if (txtUsername.Text == "" || txtFirstName.Text == "" || txtLastName.Text == "" || txtEmail.Text == "")
            {

                MessageBox.Show("No se pueden ingresar datos en blanco");
            }
            else {

                // hacemos el insert al servicio de odata
                Insertar(txtUsername.Text, txtFirstName.Text, txtLastName.Text, txtEmail.Text);
                // Limpiamos los textboxes al cargar datos
                txtUsername.Text = "";
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtEmail.Text = "";


                MessageBox.Show("Datos agregados.");

            }



            
        }


        // Funcion que trae la informacion de Odata
        public void LoadDB() {

            
            // Busca el singleton del conector
            var people = ABMClass.Connector().People.ToList();
           
            if (dtgv_Grid.RowCount != 0)
                dtgv_Grid.Rows.Clear();

            for (int i = 0; i < people.Count; i++)
            {
                //agregamos nueva fila
                dtgv_Grid.Rows.Add();

                //Colocamos la informacion en cada una de las filas creadas
                dtgv_Grid.Rows[i].Cells[0].Value = people[i].UserName;
                dtgv_Grid.Rows[i].Cells[1].Value = people[i].FirstName;
                dtgv_Grid.Rows[i].Cells[2].Value = people[i].LastName;
                dtgv_Grid.Rows[i].Cells[3].Value = people[i].Photo;
                dtgv_Grid.Rows[i].Cells[4].Value = people[i].Trips.Count;

                var peopleFriends = ABMClass.Connector().People.Expand(p => p.Friends);

                var peopleFriendList = peopleFriends.ToList();
                
                // armo un string con los nombre de usuarios de la lista de amigos
                string combinedFriends = "";
                foreach (var item in peopleFriendList)
                {
                    combinedFriends += item.UserName + " / ";

                }
                dtgv_Grid.Rows[i].Cells[5].Value = combinedFriends;

                dtgv_Grid.Rows[i].Cells[6].Value = people[i].Gender;

                string combindedStringEmail = string.Join(",", people[i].Emails.ToArray());


                dtgv_Grid.Rows[i].Cells[7].Value = combindedStringEmail;
                dtgv_Grid.Rows[i].Cells[8].Value = people[i].Concurrency;
                
            }
        }


        public void Buscar(string texto) {
            //Realizamos la consulta de busqueda

            if (dtgv_Grid.RowCount != 0)
            {
                dtgv_Grid.Rows.Clear();
            }
            
            // Filtramos por el texto ingresado en el campo de busqueda
            var people = from p in ABMClass.Connector().People where p.UserName.Contains(texto) select p;
            var persons = people.ToList();

            for (int i = 0; i < persons.Count; i++)
            {
                
                    dtgv_Grid.Rows.Add();

                    //Colocamos la informacion en cada una de las filas creadas
                    dtgv_Grid.Rows[0].Cells[0].Value = persons[i].UserName;
                    dtgv_Grid.Rows[0].Cells[1].Value = persons[i].FirstName;
                    dtgv_Grid.Rows[0].Cells[2].Value = persons[i].LastName;
                    dtgv_Grid.Rows[0].Cells[3].Value = persons[i].Photo;
                    dtgv_Grid.Rows[0].Cells[4].Value = persons[i].Trips.Count;

                    // string combindedStringFriend = string.Join(",", persons[i].Friends);

                    var peopleFriends = ABMClass.Connector().People.Expand(p => p.Friends);

                    var peopleFriendList = peopleFriends.ToList();
                    
                    // generamos un string con los nombre de usuarios de la lista de amigos
                    string combinedFriends = "";
                    foreach (var item in peopleFriendList)
                    {
                        combinedFriends += item.UserName + " / ";

                    }
                    dtgv_Grid.Rows[0].Cells[5].Value = combinedFriends;


                    dtgv_Grid.Rows[0].Cells[5].Value = combinedFriends;
                    dtgv_Grid.Rows[0].Cells[6].Value = persons[i].Gender;

                    string combindedStringEmail = string.Join(",", persons[i].Emails.ToArray());


                    dtgv_Grid.Rows[0].Cells[7].Value = combindedStringEmail;
                    dtgv_Grid.Rows[0].Cells[8].Value = persons[i].Concurrency;
   
            }
        }

        // Este codigo permite insertar una nueva fila de datos
        public void Insertar(string username, string firstname, string lastname, string email) {


            //n = ABMClass.Connector().People.ToList().Count();
            int n = dtgv_Grid.RowCount;

            var newPerson = new Person()
            {
                UserName = username,
                FirstName = firstname,
                LastName = lastname,
                Emails = new ObservableCollection<string>() { email },
                AddressInfo =
                    new ObservableCollection<Location>()
                    {
            new Location()
            {
                Address = "187 Suffolk Ln.",
                City = new City() {CountryRegion = "United States", Name = "Boise", Region = "ID"}
            }
                    },
                Gender = PersonGender.Male,
                Concurrency = 635519729375200400
            };
            ABMClass.Connector().AddObject("People", newPerson);
            DataServiceResponse respuesta = ABMClass.Connector().SaveChanges();

            Console.WriteLine(respuesta.BatchStatusCode);
            Console.ReadLine();


            // TripPin es de solo lectura por lo que forzamos agregar los datos en la siguiente fila

            //Adicionamos nueva fila
            dtgv_Grid.Rows.Add();

            //Colocamos la informacion
            dtgv_Grid.Rows[n].Cells[0].Value = txtUsername.Text;
            dtgv_Grid.Rows[n].Cells[1].Value = txtFirstName.Text;
            dtgv_Grid.Rows[n].Cells[2].Value = txtLastName.Text;
            dtgv_Grid.Rows[n].Cells[7].Value = txtEmail.Text;

        }

        private void dtgv_Grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        // Referencia de la celda de grilla
        private void dtgv_Grid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // Guardo en una variable la celda seleccionada
            n = e.RowIndex;

            // Si seleciono una celda muestro la informacion en el label info
            if (n != -1)
            {
                lblInformacion.Text = (string)dtgv_Grid.Rows[n].Cells[1].Value;

                var context = ABMClass.Connector();

                // Modifica la celda de username
                var people = context.People.Where(p => p.UserName == (string)dtgv_Grid.Rows[n].Cells[0].Value).SingleOrDefault();
                if (people != null)
                {
                    people.UserName = (string)dtgv_Grid.Rows[n].Cells[0].Value;
                    context.UpdateObject(people);
                    context.SaveChanges();
                    
                }


            }
        }

        // Boton que se encarga de borrar una fila de datos
        private void btnBorrar_Click(object sender, EventArgs e)
        {
            // Si selecciono una fila diferentes del titulo intenta borrarla
            if (n != -1) {


                    var people = ABMClass.Connector().People.Where(p => p.UserName == lblUsername.Text).SingleOrDefault();
                    if (people != null)
                    {
                        ABMClass.Connector().DeleteObject(people);
                        ABMClass.Connector().SaveChanges();
                    }
                    
                    dtgv_Grid.Rows.RemoveAt(n);
                    
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            LoadDB();

        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            if (txtBuscar.Text != "")
            {
                Buscar(txtBuscar.Text);
                btnAdicionar.Enabled = false;
                btnBorrar.Enabled = false;
                btnUpdate.Enabled = false;
            }
            else
            {
                LoadDB();
                btnAdicionar.Enabled = true;
                btnBorrar.Enabled = true;
                btnUpdate.Enabled = true;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
